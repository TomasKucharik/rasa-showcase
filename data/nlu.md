## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- hello there
- sup

## intent:goodbye
- bye
- goodbye
- see you around
- see you later

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct
- yeah
- yea
- yup

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- nope
- i dont know
- i do not know

## intent:question_rasa_parts
- what are the basic parts of rasa
- what are the parts of rasa
- does rasa have any smaller parts
- what is rasa made of

## intent:question_intents
- what are intents in rasa
- what does intent mean?
- can you explain what intent means? 
- do you know what is intent?
- do you know what are intents?
- tell me what is intent?

## intent:question_out_of_scope
- what is the weather?
- what is the time?
- what is the date today?

## intent:question_rasa
- what is rasa?
- what is rasa?
- i dont know what rasa is
- can you explain to me what rasa is?
- i have no idea what is rasa
- rasa?
- i do not know what is rasa
- tell me what is rasa

## intent:question_rasa_version
- what is the latest rasa version?
- what is the current rasa version?
- what is the rasa version?
- can you tell me what is the rasa version?

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?

## intent:meaning_of_life
- what is the meaning of life? 
- what is the life's meaning?
- do you know whats the meaning of life? 
- do you know what is the meaning of life?
