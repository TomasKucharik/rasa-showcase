from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import requests

class ActionCheckRasaVersion(Action):

    def name(self) -> Text:
        return "action_check_rasa_version"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        URL = "https://pypi.org/pypi/rasa/json"
        
        # defining a params dict for the parameters to be sent to the API 
        PARAMS = {
            'Host': 'pypi.org',
            'Accept': 'application/json'
        } 
        
        # sending get request and saving the response as response object 
        r = requests.get(url = URL, params = PARAMS) 
        
        # extracting data in json format 
        data = r.json() 

        dispatcher.utter_message(text="The latest Rasa version is " + data['info']['version'])

        return []
